# Stripe

**stripe** est un outil qui va vous permettre de mettre en place un système de paiement bancaires directement sur votre (Site, app).

[Site officiel](https://stripe.com/fr)

---
## Aventage
 - le client va rentrer ces cordonnées bancaires directement sur votre formulaire, il n'aura pas besoin de quitter pour accepter les paiements.
 - vous n'avez rien de sensible à sauvegarder (numéro de la carte ..)).
---
## fonctionement
- soumettre les informations via un formulaire classique (numero de la carte bancaire, date EX, code de vérification)
- lorsque ces informations vont être soumises elles vont directement au serveur stripe
- stripe vous renvoi un token que vous pouvez utiliser pour créer un client et ensuite sauvegarder l'ID de ce client dans votre BDD pour des paiement plus tard.

## mettre en place 
- creation d'un compte Stripe [Inscription](https://dashboard.stripe.com/register)
- ajout des dependances suivante : implementation 'com.stripe:stripe-android:15.1.0', implementation 'com.squareup.okhttp3:okhttp:4.4.0', 
   implementation 'com.google.code.gson:gson:2.8.6'
- documentation pour toute type d'integration [documentation](https://stripe.com/docs/payments/cards/overview)